import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Dobla l'enter
*/
fun main() {
    val sc = Scanner(System.`in`)
    println("Introduce un valor entero:")
    val intValue = sc.nextInt()
    println("El resultado es: ${intValue*2}")
}