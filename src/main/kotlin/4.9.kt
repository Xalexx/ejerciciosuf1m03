/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Valors repetits
*/
fun main(args: Array<String>) {

    for (arg in args.indices){
        for (j in arg + 1 until args.size){
            if (args[arg] == args[j]){
                println(args[arg])
            }
        }
    }
}