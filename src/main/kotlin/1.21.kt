import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Tres nombres iguals
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()

    println(firstNumber == secondNumber && firstNumber == thirdNumber)
}