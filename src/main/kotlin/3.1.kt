import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Pinta X numeros
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    for (i in 1..number) println(i)
}