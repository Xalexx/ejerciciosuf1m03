import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Es primer?
*/

fun checkPrime(num: Int): Boolean {
    var flag = false
    for (i in 2..num / 2) {
        if (num % i == 0) {
            flag = true
            break
        }
    }
    return flag
}

fun main() {
    val sc = Scanner(System.`in`)

    val num = sc.nextInt()

    if (!checkPrime(num)) println("$num es un numero primo")
    else println("$num no es un numero primo")
}

