import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Elimina repeticions
*/

fun main() {
    val sc = Scanner(System.`in`)

    val list = MutableList(sc.nextInt()){sc.nextInt()}
    val secondList = mutableListOf<Int>()

    for (i in list.indices){
        if (list[i] !in secondList){
            secondList.add(list[i])
        }
    }
    for (i in secondList){
        print("$i ")
    }
}