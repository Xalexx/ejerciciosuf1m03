import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Conversor d'unitats
*/
fun main() {
    val sc = Scanner(System.`in`)

    val weight = sc.nextDouble()
    val unity = sc.next()

    if (unity == "G"){
        println("${weight / 1000000.0} TN\n" +
                "${weight / 1000.0} KG")
    }
    if (unity == "KG"){
        println("${weight / 1000.0} TN\n" +
                "${weight * 1000.0} G")
    }
    if (unity == "TN"){
        println("${weight * 1000.0} KG\n" +
                "${weight * 1000000.0} G")
    }
}