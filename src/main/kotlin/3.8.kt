import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Nombre de digits
*/

fun main() {
    val sc = Scanner(System.`in`)

    var number = sc.nextInt()
    var score = 0

    if (number == 0) score++
    else {
        while (number != 0) {
            number /= 10
            score++
        }
    }
    println(score)


}