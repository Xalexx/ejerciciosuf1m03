import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Es vocal o consonant?
*/
fun main() {
    val sc = Scanner(System.`in`)

    val letter = sc.next().single()

    when (letter.lowercaseChar()){
        'a' -> println("Vocal")
        'e' -> println("Vocal")
        'i' -> println("Vocal")
        'o' -> println("Vocal")
        'u' -> println("Vocal")
        else -> println("Consonant")
    }
}