import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Pupitres
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstClass = sc.nextInt()
    val secondClass = sc.nextInt()
    val thirdClass = sc.nextInt()

    val opFirstClass = (firstClass / 2) + (firstClass % 2)
    val opSecondClass = (secondClass / 2) + (secondClass % 2)
    val opThirdClass = (thirdClass / 2) + (thirdClass % 2)

    println(opFirstClass + opSecondClass + opThirdClass)
}