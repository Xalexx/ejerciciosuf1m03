import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Es un bitllet valid
*/

fun main() {
    val sc = Scanner(System.`in`)

    when (sc.nextInt()) {
        5, 10, 20, 50, 100, 200, 500 -> println(true)
        else -> println(false)
    }
}
