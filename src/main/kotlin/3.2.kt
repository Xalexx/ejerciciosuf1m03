import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Calcula la suma dels N primers
*/

fun main() {
    val sc = Scanner(System.`in`)

    var contador = 0
    val number = sc.nextInt()

    for (i in 1..number){
        contador += i
    }
    println(contador)
}