import java.util.Scanner
import kotlin.math.max
import kotlin.math.min

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/7
* TITLE: Extrems
*/

fun main() {
    val sc = Scanner(System.`in`)

    var numbers = sc.nextInt()

    var bool = false

    var minNumber = numbers
    var maxNumber = numbers

    while (!bool) {
        numbers = sc.nextInt()

        if (numbers == 0) bool = true
        else {
            if (minNumber < numbers){
                minNumber = numbers
            }
            if (maxNumber > numbers){
                maxNumber = numbers
            }
        }
    }
    println("$minNumber $maxNumber")
}