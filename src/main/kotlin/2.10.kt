import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/7
* TITLE: Calcula la lletra del DNI
*/
fun main() {
    val sc = Scanner(System.`in`)

    val dniNumber = sc.nextInt()

    when(dniNumber % 23){
        0 -> println("${dniNumber}T")
        1 -> println("${dniNumber}R")
        2 -> println("${dniNumber}W")
        3 -> println("${dniNumber}A")
        4 -> println("${dniNumber}G")
        5 -> println("${dniNumber}M")
        6 -> println("${dniNumber}Y")
        7 -> println("${dniNumber}F")
        8 -> println("${dniNumber}P")
        9 -> println("${dniNumber}D")
        10 -> println("${dniNumber}X")
        11 -> println("${dniNumber}B")
        12 -> println("${dniNumber}N")
        13 -> println("${dniNumber}J")
        14 -> println("${dniNumber}Z")
        15 -> println("${dniNumber}S")
        16 -> println("${dniNumber}Q")
        17 -> println("${dniNumber}V")
        18 -> println("${dniNumber}H")
        19 -> println("${dniNumber}L")
        20 -> println("${dniNumber}C")
        21 -> println("${dniNumber}K")
        22 -> println("${dniNumber}E")
    }
}