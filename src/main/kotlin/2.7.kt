import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Parell o Senar
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    if (number % 2 == 0) println("Parell")
    else println("Senar")
}