import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: La suma total
*/

fun main() {
    val sc = Scanner(System.`in`)

    val list = List(sc.nextInt()){sc.nextInt()}
    var result = 0
    var verify = false

    for (i in list.indices){
        for (j in list.indices){
            if (i != j) result += list[j]
        }
        if (result == list[i]){
            println("si")
            verify = true
            break
        } else result = 0
    }
    if (!verify) println("no")
}