/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: Calcula la mitjana
*/
fun main(args: Array<String>) {

    var sum = 0.0

    for (i in args){
        val arg = i.toDouble()
        sum += arg
    }
    println(sum / args.size)
}