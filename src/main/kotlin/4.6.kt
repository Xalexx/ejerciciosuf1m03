import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: Minim i Maxim
*/
fun main(args: Array<String>) {

    var min = args[0].toInt()
    var max = args[0].toInt()

    for (arg in args){

        val i = arg.toInt()

        if (i < min) min = i
        else if (i > max) max = i
    }
    print("$min $max")
}