import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Divisor de comptpe
*/

fun main() {
    val sc = Scanner(System.`in`)

    val clientes = sc.nextInt()
    val costeTotal = sc.nextDouble()

    println("${costeTotal/clientes} €")
}
