import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: Triangle de nombres
*/

/*
En matemáticas, un número primo es un número natural mayor que 1 que tiene únicamente dos divisores positivos distintos:
    él mismo y el 1. Por el contrario, los números compuestos son los números naturales que tienen algún divisor
    natural aparte de sí mismos y del 1, y, por lo tanto, pueden factorizars
*/
fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    for (i in 1..number){
        for (j in 1..i){
            print(j)
        }
        println()
    }
}
