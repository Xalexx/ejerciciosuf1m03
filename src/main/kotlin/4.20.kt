import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    val firstADN = sc.next()
    val secondADN = sc.next()

    var hammingCount = 0

    if (firstADN.length != secondADN.length) println("Entrada no vàlida")
    else {
        for (i in 0..firstADN.lastIndex){
            if (firstADN[i] != secondADN[i]) hammingCount++
        }
        println(hammingCount)
    }
}