import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Nombes decimals iguals
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextDouble()
    val secondNumber = sc.nextDouble()

    println(firstNumber == secondNumber)
}