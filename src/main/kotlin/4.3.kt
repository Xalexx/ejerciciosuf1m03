import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: Calcula la lletra del DNI
*/
fun main() {
    val sc = Scanner(System.`in`)

    val letter = arrayOf('T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E')

    val dniNumber = sc.nextInt()
    val dniRest = dniNumber%23
    
    println("$dniNumber${letter[dniRest]}")
}