import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Es edat legal
*/

fun main() {
    val sc = Scanner(System.`in`)

    val age = sc.nextInt()

    println(age >= 18)
}