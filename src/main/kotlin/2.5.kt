import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: En rang
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()
    val fourNumber = sc.nextInt()
    val fiveNumber = sc.nextInt()

    if (fiveNumber in firstNumber..secondNumber && fiveNumber in thirdNumber..fourNumber){
        println(true)
    } else println(false)
}