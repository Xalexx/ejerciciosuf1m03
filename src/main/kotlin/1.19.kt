import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Programa Adolescent
*/

fun main() {
    val sc = Scanner(System.`in`)

    val value = sc.nextBoolean()

    println(!value)
}