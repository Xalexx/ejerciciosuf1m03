import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/7
* TITLE: Canvi mínim
*/

fun main() {
    val sc = Scanner(System.`in`)

    var euros = sc.nextInt()
    var centimos = sc.nextInt()

    var b500 = 0
    var b200 = 0
    var b100 = 0
    var b50 = 0
    var b20 = 0
    var b10 = 0
    var b5 = 0
    var m2 = 0
    var m1 = 0
    var mc50 = 0
    var mc20 = 0
    var mc10 = 0
    var mc5 = 0
    var mc2 = 0
    var mc1 = 0

    if (euros % 500 < 500){
        b500 = euros / 500
        euros -= ((euros / 500) * 500)
    }
    if (euros % 200 < 200){
        b200 = euros / 200
        euros -= ((euros / 200) * 200)
    }
    if (euros % 100 < 100){
        b100 = euros / 100
        euros -= ((euros / 100) * 100)
    }
    if (euros % 50 < 50){
        b50 = euros / 50
        euros -= ((euros / 50) * 50)
    }
    if (euros % 20 < 20){
        b20 = euros / 20
        euros -= ((euros / 20) * 20)
    }
    if (euros % 10 < 10){
        b10 = euros / 10
        euros -= ((euros / 10) * 10)
    }
    if (euros % 5 < 5){
        b5 = euros / 5
        euros -= ((euros / 5) * 5)
    }
    if (euros % 2 < 2){
        m2 = euros / 2
        euros -= ((euros / 2) * 2)
    }
    if (euros % 1 < 1){
        m1 = euros / 1
        euros -= ((euros / 1) * 1)
    }
    if (centimos % 50 < 50){
        mc50 = centimos / 50
        centimos -= ((centimos / 50) * 50)
    }
    if (centimos % 20 < 20){
        mc20 = centimos / 20
        centimos -= ((centimos / 20 * 20))
    }
    if (centimos % 10 < 10){
        mc10 = centimos / 10
        centimos -= ((centimos / 10) * 10)
    }
    if (centimos % 5 < 5){
        mc5 = centimos / 5
        centimos -= ((centimos / 5) * 5)
    }
    if (centimos % 2 < 2){
        mc2 = centimos / 2
        centimos -= ((centimos / 2) * 2)
    }
    if (centimos % 1 < 1){
        mc1 = centimos / 1
        centimos -= ((centimos / 1) * 1)
    }
    println(euros % 500)

    println("Bitllets de 500 euros: $b500 \n" +
            "Bitllets de 200 euros: $b200 \n" +
            "Bitllets de 100 euros: $b100 \n" +
            "Bitllets de 50 euros: $b50 \n" +
            "Bitllets de 20 euros: $b20 \n" +
            "Bitllets de 10 euros: $b10 \n" +
            "Bitllets de 5 euros: $b5 \n" +
            "Monedes de 2 euros: $m2 \n" +
            "Monedes de 1 euro: $m1 \n" +
            "Monedes de 50 centims: $mc50 \n" +
            "Monedes de 20 centims: $mc20 \n" +
            "Monedes de 10 centims: $mc10 \n" +
            "Monedes de 5 centims: $mc5 \n" +
            "Monedes de 2 centims: $mc2 \n" +
            "Monedes de 1 centims: $mc1")

}