/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Ordena l'array (DOWN)
*/
fun main(args: Array<String>) {

    var aux : String

    for (i in args.indices){
        for (j in i + 1 until args.size){
            if (args[i].toInt() < args[j].toInt()) {
                aux = args[i]
                args[i] = args[j]
                args[j] = aux
            }
        }
    }
    for (arg in args) print("$arg ")
}
