import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/13
* TITLE: Envdevina el numero (Ara amb intents)
*/

fun main() {
    val sc = Scanner(System.`in`)

    val secretNumber = (Math.random() * (100 - 1) + 1).toInt()
    var userNumber : Int
    var attempts = 0

    do {
        userNumber = sc.nextInt()
        attempts++
        if (attempts == 6){
            println("Has perdut :(")
            break
        }
        if (userNumber == secretNumber){
            println("Has encertat!")
        } else if (userNumber < secretNumber){
            println("Massa baix!")
        } else println("Massa alt!")
    } while (userNumber != secretNumber)

    userNumber.toString()
}