import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Inverteix l'array
*/
fun main() {
    val sc = Scanner(System.`in`)

    val listOfNumbers = List(sc.nextInt()){sc.nextInt()}

    for (i in listOfNumbers.lastIndex downTo 0){
        print("${listOfNumbers[i]} ")
    }
}