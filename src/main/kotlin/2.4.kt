import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Te edat per treballar
*/

fun main() {
    val sc = Scanner(System.`in`)

    val age = sc.nextInt()

    if (age in 16..65){
        println("Te edat per treballar")
    } else println("No te edat per treballar")
}