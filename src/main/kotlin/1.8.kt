import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Dobla el decimal
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextDouble()

    println(number*2)
}