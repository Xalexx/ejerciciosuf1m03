import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Quina és la mida de la meva pizza?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val diameter = sc.nextDouble()

    val radius = diameter/2
    val lastOperation = Math.PI * (radius * radius)

    println(lastOperation)

}