import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Calcula l'àrea
*/
fun main() {
    val sc = Scanner(System.`in`)
    val width = sc.nextInt()
    val height = sc.nextInt()

    println("El area de la habitacion es la siguiente: ${width*height}")
}