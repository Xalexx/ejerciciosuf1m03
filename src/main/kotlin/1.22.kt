import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Qui riu ultim riu millor
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()
    val fourNumber = sc.nextInt()
    val fiveNumber = sc.nextInt()

    println(fiveNumber > fourNumber && fiveNumber > thirdNumber && fiveNumber > secondNumber && fiveNumber > firstNumber)
}