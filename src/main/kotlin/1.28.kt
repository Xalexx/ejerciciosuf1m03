import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Un es 10
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()
    val fourNumber = sc.nextInt()

    println(firstNumber == 10 || secondNumber == 10 || thirdNumber == 10 || fourNumber == 10)
}