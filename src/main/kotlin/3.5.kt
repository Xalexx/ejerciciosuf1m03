import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Taula Multiplicar
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    for (i in 1..10){
        println("$number x $i = ${number*i}")
    }
}
