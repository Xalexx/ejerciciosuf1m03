    import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Salari
*/

fun main() {
    val sc = Scanner(System.`in`)

    val hours = sc.nextInt()
    val extraHours = hours-40

    if (hours <= 40){
        println(hours * 40)
    } else {
        println((40*40)+((extraHours)*(40*1.5)).toInt())
    }
}