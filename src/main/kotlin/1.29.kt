import java.util.*
import kotlin.math.sqrt

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Equacions de segon grau
*/

fun main() {
    val sc = Scanner(System.`in`)

    val a = sc.nextInt().toDouble()
    val b = sc.nextInt().toDouble()
    val c = sc.nextInt().toDouble()

    val firstFormula = (-b - (sqrt(b*b - 4*a*c))) / (2*a)
    val secondFormula = (-b + (sqrt(b*b - 4*a*c))) / (2*a)

    println(firstFormula.toFloat())
    println(secondFormula.toFloat())
}