import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Cuants dies te el mes?
*/
fun main() {
    val sc = Scanner(System.`in`)

    when (sc.nextInt()){
        1, 3, 5, 7, 8, 10, 12 -> println("31")
        4, 6, 9, 11 -> println("30")
        2 -> println("28")
        else -> println("Numero del mes incorrecte")
    }
}