import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    var word = sc.next()
    val firstChar = sc.next().single()
    val secondChar = sc.next().single()

    if (word.contains(firstChar)) word = word.replace("$firstChar", "$secondChar")

    println(word)
}