import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/11/4
* TITLE: Triange invertit d'*
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    for (i in number downTo 1){
        for (j in number downTo 1){
            if (j>i) print(" ")
            else print("*")
        }
        println()
    }
}