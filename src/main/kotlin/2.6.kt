import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Quina pizza es mes gran
*/

fun main() {
    val sc = Scanner(System.`in`)

    val diameter = sc.nextInt()
    val firstSide = sc.nextInt()
    val secondSide = sc.nextInt()


    val radius = diameter/2
    val circleArea = Math.PI * (radius*radius)
    val rectangleArea = firstSide * secondSide

    if (circleArea > rectangleArea){
        println("Pizza rodona: $circleArea")
    } else println("Pizza rectangular: $rectangleArea")
}