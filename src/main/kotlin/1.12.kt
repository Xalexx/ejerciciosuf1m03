import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: De Celsius a Fahrenheit
*/

fun main() {
    val sc = Scanner(System.`in`)

    val celsius = sc.nextDouble()
    val result = (celsius * 9 / 5) + 32

    println("El resultat en graus Fahrenheit es: $result")
}