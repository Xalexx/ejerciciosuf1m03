import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Calcula el descompte
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextDouble()
    val secondNumber = sc.nextDouble()

    val firstOpration = (firstNumber/secondNumber)
    val lastOperaton = 100-(100/firstOpration)

    println("${lastOperaton}%")
}