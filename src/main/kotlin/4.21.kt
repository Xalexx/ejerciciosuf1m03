import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    var sequence = sc.next()

    for (i in 0..sequence.lastIndex){
        for (j in 0..sequence.lastIndex){
            if (i != j){
                if (sequence[i] == '(' && sequence[j] == ')'){
                    sequence = sequence.replace("${sequence[i]}", "")
                    sequence = sequence.replace("${sequence[j]}", "")
                }
            }
        }
    }
    if (sequence.isEmpty()) println("si")
    else println("no")
}