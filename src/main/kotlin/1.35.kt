import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/27
* TITLE: Creador de targetes de treball
*/

fun main() {
    val sc = Scanner(System.`in`)

    val name = sc.next()
    val surname = sc.next()
    val department = sc.nextInt()

    println("Empleada: $name $surname - Despatx: $department")
}
