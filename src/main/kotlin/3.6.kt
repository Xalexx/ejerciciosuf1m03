import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Eleva'l
*/

fun main() {
    val sc = Scanner(System.`in`)

    val base = sc.nextInt()
    val exponente = sc.nextInt()
    var result = 1L

    for (i in 1..exponente){
        result *= base
    }
    println(result)
}

