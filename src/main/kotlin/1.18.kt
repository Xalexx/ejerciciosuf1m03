import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Es major que
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    println(firstNumber > secondNumber)
}