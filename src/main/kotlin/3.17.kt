import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/13
* TITLE: Factorial!
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()
    var result = 1

    for (i in number downTo 1){
        result *= i
    }

    println(result)
}