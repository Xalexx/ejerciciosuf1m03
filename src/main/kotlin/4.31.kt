import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Comptant a's
*/

fun main() {
    val sc = Scanner(System.`in`)

    val userInput = sc.nextLine()
    val list = mutableListOf<Char>()

    for (i in userInput) {
        if (i == '.') break
        if (i == 'a') list.add(i)
    }
    println(list.size)
}