import java.util.Scanner
import kotlin.math.abs
import kotlin.math.absoluteValue

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Valor absolut
*/
fun main() {
    val sc = Scanner(System.`in`)

    val value = sc.nextInt()

    println(abs(value))
}