import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Afegeix un segon
*/

fun main() {
    val sc = Scanner(System.`in`)

    var hour = sc.nextInt()
    var minutes = sc.nextInt()
    var seconds = sc.nextInt()

    seconds++

    if (hour == 23 && minutes == 59 && seconds == 60){
        println("00:00:00")
    } else if (minutes == 59 && seconds == 60){

        hour++
        if (hour < 10) println("0$hour:00:00")
        else println("$hour:00:00")

    } else if (minutes < 59 && seconds == 60){

        minutes++
        if (minutes < 10) println("0$hour:0$minutes:00")
        else println("$hour:$minutes:00")

    } else if (seconds < 10) println("$hour:$minutes:0$seconds")
    else println("$hour:$minutes:$seconds")
}