import java.util.Scanner

/**
 * AUTHOR Alejandro Arcas Leon
 * DATE: 2022/10/13
 * TITLE: Coordenades en moviment
*/

fun main() {
    val sc = Scanner(System.`in`)

    var userInput : Char

    var positionA = 0
    var positionB = 0

    do {
        userInput = sc.next().single()
        when (userInput){
            'n' -> positionB--
            's' -> positionB++
            'e' -> positionA++
            'o' -> positionA--
        }

    } while (userInput != 'z')
    println("($positionA, $positionB)")
}
