import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    val word = sc.next()

    var normalWord = ""
    var reverseWord = ""

    for (i in 0..word.lastIndex){
        normalWord += word[i]
    }
    for (i in word.lastIndex downTo 0){
        reverseWord += word[i]
    }
    if (normalWord == reverseWord) println("És un palindrom")
    else println("No és un palindrom")
}