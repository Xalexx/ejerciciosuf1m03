import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Quina temperatura fa?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val numTemp = sc.nextDouble()
    val numIncr = sc.nextDouble()

    println("La temperatura actual es: ${numTemp + numIncr}º")
}