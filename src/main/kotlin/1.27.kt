import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Som igualas?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstChar = sc.next().single()
    val secondChar = sc.next().single()

    println(firstChar.uppercase() == secondChar.uppercase())
}

