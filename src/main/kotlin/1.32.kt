import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Calcula el capital, no LA Capital
*/

fun main() {
    val sc = Scanner(System.`in`)

    val capital = sc.nextDouble()
    val years = sc.nextInt()
    val interessos = sc.nextInt()

    val operation = capital*((interessos+100)*years)

    println(operation)
}
