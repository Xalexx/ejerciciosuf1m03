import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Puges o baixes?
*/
fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()

    if (thirdNumber > secondNumber && secondNumber > firstNumber){
        println("Ascendent")
    } else if (firstNumber > secondNumber && secondNumber > thirdNumber){
        println("Descendent")
    } else println("Cap de les dues")
}
