import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/12
* TITLE: Envdevina el numero
*/
fun main() {
    val sc = Scanner(System.`in`)

    val secretNumber = (Math.random() * (100 - 1) + 1).toInt()
    var userNumber : Int

    do {
        userNumber = sc.nextInt()
        if (userNumber == secretNumber){
            println("Has encertat!")
        } else if (userNumber < secretNumber){
            println("Massa baix!")
        } else println("Massa alt!")
    } while (userNumber != secretNumber)
}