import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Es una lletra?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val letter = sc.next().single()

    println(letter>='a' && letter<='z' || letter>='A' && letter<='Z')
}
