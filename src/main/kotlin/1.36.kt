import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/27
* TITLE: Construeix la historia
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstSentence = sc.nextLine()
    val secondSentence = sc.nextLine()
    val thirdSentence = sc.nextLine()

    println("$firstSentence $secondSentence $thirdSentence")
}