import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Imprimeix el Rang
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    if (firstNumber <= secondNumber){
        for (i in firstNumber until secondNumber){
            print("$i,")
        }
    } else println("ERROR: El primer nombre no pot ser mes gran que el segon")
    print(secondNumber)
}