import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/27
* TITLE: Hola Usuari
*/

fun main() {
    val sc = Scanner(System.`in`)

    val inputName = sc.next()

    println("Bon dia $inputName")
}