/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: Suma els valors
*/
fun main(args: Array<String>) {

    var result = 0

    for (arg in args){
        val i = arg.toInt()
        result += i
    }
    println(result)
}