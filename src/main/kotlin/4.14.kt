import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Quants sumen...?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val listOfNumbers = listOf(25, 5, 2, 10, 33, 15, 40, 1, 20, 4, 11)

    val userValue = sc.nextInt()

    for (i in listOfNumbers.indices){
        for (j in listOfNumbers.indices){
            if (j != i){
                if (listOfNumbers[i] + listOfNumbers[j] == userValue) println("${listOfNumbers[i]} ${listOfNumbers[j]}")
            }
        }
    }
}