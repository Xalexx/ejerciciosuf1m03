import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Imprimeix el Rang (2)
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    if (firstNumber <= secondNumber){
        for (i in firstNumber until secondNumber){
            print("$i,")
        }
        print(secondNumber)
    } else {
        for (i in firstNumber downTo secondNumber){
            print("$i ")
        }
    }
}