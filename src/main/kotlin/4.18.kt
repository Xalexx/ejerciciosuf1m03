import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Purga de caracters
*/

fun main() {
    val sc = Scanner(System.`in`)

    var word = sc.next()
    var chars = ' '

    while (chars != '0'){
        chars = sc.next().single()
        if (word.contains(chars)) word = word.replace("$chars", "")
    }
    println(word)
}