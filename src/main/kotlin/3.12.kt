import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/12
* TITLE: Divisible per 3 i per 5
*/

fun main() {
    val sc = Scanner(System.`in`)

    val maxNumber = sc.nextInt()

    for (i in 1..maxNumber){
        if (i % 3 == 0 && i % 5 == 0){
            println("$i es divisible per 3 i per 5")
        } else if (i % 3 == 0){
            println("$i es divisible per 3")
        } else if (i % 5 == 0){
            println("$i es divisible per 5")
        }
    }
}