import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: De metre a Peu
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt().toFloat()

    val operation = (number*39.37)/12.0

    println(operation)
}