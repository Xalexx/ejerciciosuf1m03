import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/14
* TITLE: En quina posició
*/
fun main() {
    val sc = Scanner(System.`in`)

    val listOfNumbers = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val userInput = sc.nextInt()

    if (userInput in listOfNumbers) {
        println(listOfNumbers.indexOf(userInput))
    }
    else println("No esta contingut")
}