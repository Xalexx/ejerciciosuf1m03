import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/11/22
* TITLE: Inverteix les paraules
*/

fun main() {
    val sc = Scanner(System.`in`)

    val words = sc.nextLine()

    for (i in words.length-1 downTo 0){
        print(words[i])
    }

}