import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Transforma l'enter
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    println(number * 1.0)
}