import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Son iguals? (2)
*/
fun main() {
    val sc = Scanner(System.`in`)

    val firstUserInput = sc.next()
    val secondUserInput = sc.next()

    if (firstUserInput == secondUserInput) println("Son iguals")
    else println("No son iguals")
}