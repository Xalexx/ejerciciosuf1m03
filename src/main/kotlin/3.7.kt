import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/6
* TITLE: Revers de l'enter
*/

fun main() {
    val sc = Scanner(System.`in`)

    var number = sc.nextInt()
    var list = mutableListOf<Int>()

    while(number > 0){
        list.add(number % 10);
        number /= 10;
    }

    for (i in 0..list.lastIndex){
        print(list[i])
    }
}

