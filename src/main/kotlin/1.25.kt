import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Fes-me majuscula
*/

fun main() {
    val sc = Scanner(System.`in`)

    val char = sc.next().single()

    println(char.uppercase())
}