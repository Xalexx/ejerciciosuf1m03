import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Afegeix un segon
*/

fun main() {
    val sc = Scanner(System.`in`)

    val seconds = sc.nextInt()
    println((seconds+1)%60)
}