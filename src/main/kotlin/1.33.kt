import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/27
* TITLE: És divisible
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    println(firstNumber % secondNumber == 0 || secondNumber % firstNumber == 0)
}
