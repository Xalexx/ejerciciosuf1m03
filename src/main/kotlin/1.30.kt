import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Quant de temps?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val number = sc.nextInt()

    val hour = (number/60)/60
    val minutes = (number/60)-60
    val seconds = number-((minutes*60)+((hour*60)*60))


    println("$hour hora $minutes minutos $seconds segundos")
}