import java.util.*
/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Quina nota he tret?
*/
fun main() {
    val sc = Scanner(System.`in`)

    val note = sc.nextDouble()

    when (note){
        in 9.0..10.0 -> println("Excelent")
        in 7.0..8.9 -> println("Notable")
        in 6.0..6.9 -> println("Bé")
        in 5.0..5.9 -> println("Suficient")
        in 0.0..4.9 -> println("Insuficient")
        else -> println("Nota invalida")
    }
}