import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/7
* TITLE: Calculadora
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()

    when (sc.next().single()) {
        '+' -> println(firstNumber + secondNumber)
        '-' -> println(firstNumber - secondNumber)
        '*' -> println(firstNumber * secondNumber)
        '/' -> println(firstNumber / secondNumber)
        '%' -> println(firstNumber % secondNumber)
    }
}