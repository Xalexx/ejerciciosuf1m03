import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/21
* TITLE: Es un nombre?
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.next().single()

    println(firstNumber>= '0' && firstNumber<= '9')
}