import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)

    val num = sc.nextInt()
    var result = 1

    for (i in 1..num){
        if (!checkPrime(i)) result *= i
    }
    println(result)
}