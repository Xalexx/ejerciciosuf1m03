import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/19
* TITLE: Numero seguent
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()

    println("Despres ve el: ${number+1}")
}