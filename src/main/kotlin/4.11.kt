/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/26
* TITLE: Quants parells i cuants senars?
*/
fun main(args: Array<String>) {

    var parells = 0
    var senars = 0

    for (arg in args){
        val i = arg.toInt()
        if (i % 2 == 0) {
            parells++
        } else senars++
    }

    println("Parells: $parells\n" +
            "Senars: $senars")
}