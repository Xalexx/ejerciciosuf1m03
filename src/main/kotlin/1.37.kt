import java.time.LocalDate
import java.util.*

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/27
* TITLE: Ajuda a la maquina de viatje del temps
*/

fun main() {
    val currentDate = LocalDate.now()

    println("Avui és $currentDate")
}
