import java.util.Scanner

/*
* AUTHOR: Alejandro Arcas Leon
* DATE: 2022/10/29
* TITLE: Maxim de 3 nombres enters
*/

fun main() {
    val sc = Scanner(System.`in`)

    val firstNumber = sc.nextInt()
    val secondNumber = sc.nextInt()
    val thirdNumber = sc.nextInt()

    if (firstNumber >= secondNumber && firstNumber >= thirdNumber){
        println(firstNumber)
    } else if (secondNumber >= firstNumber && secondNumber >= thirdNumber){
        println(secondNumber)
    } else println(thirdNumber)
}